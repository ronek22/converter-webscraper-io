# Konwerter (Postprocessing) | Webscraper.io

Wymagania:

0. Konwerter przygotowany jest w postaci skryptowej, zgodny z Python3 v=3.6 lub nowszy.


1. Konwerter na wejściu, w postaci nazwanych argumentów przyjmuje (propozycja: biblioteka argparse):
    a) `--source <input-1>,<input-2>; `lista nazw plików csv wygenerowanych przez WebScraper.io (dalej: "Źródło") oddzielonych przecinkiem,

    b) `--scheme <scheme-1>;` nazwa pliku xls wzorcowego arkusza danych (dalej: "Szablon", przesyłam przykładowy szablon w załączniku).




2. Szablon zawiera 2 wiersze definicji*:
a) Wiersz nr 1: docelowe nazwy kolumn na arkuszu wynikowym,
b) Wiesz nr 2: nazwy funkcji do wykonania na komórkach danej kolumny przy mapowanych Źródła na arkusz wynikowy (lista nazw funkcji, separator: przecinek np. 'konwertujTabliceJSON, waliduj').
pozostałe wiersze Szablonu są niewykorzystywane, nawet gdyby zawierały dodatkowe informacje


3. Konwerter w trakcie inicjalizacji tworzy 3 podstawowe struktury danych:
a) Słownik treści: nazwy kolumn z Szablonu z przypisanymi numerami kolumn (nazwa -> numer; np. 'Artykuł' -> '2'),
b) Słownik funkcji: numery kolumn (z pkt 3.a) z przypisanymi nazwami funkcji do wykonania na komórkach tej kolumny (numer -> ['funkcja1', 'funkcja2', 'funkcja3']; np. '10' -> ['konwertujTabliceJSON', 'waliduj']),
c) Słownik jednostek: skrót nazwy jednostki, z przypisaną pełną nazwą jednostki >> proszę pozostawić strukturę na razie pustą <<.


4. Konwerter implementuje zakres wymaganych funkcji (operacji wykonywanych na danych wejściowych funkcji, których rezultatem jest wyście z funkcji), obejmujących funkcje do:
a) Parsowanie informacji wejściowej przy użyciu wyrażenia regularnego; Wejście: tablica (min. 1 element), Wyjście: tablica,
b) Konwersja tablicy JSON (json2array) do tablicy python z wykorzystaniem funkcji 4.a; Wejście: dane w formie JSON, nazwa kolumny Źródła, wyrażenie regularne, Wyjście: tabela python,
c) Walidacja informacji źródłowej przy użyciu wyrażenia regularnego (w przypadku niepoprawnej wartości, obiekt jest usuwany z tablicy wyjściowej); Wejście do funkcji: tablica (min. 1 element), Wyjście: tablica,
d) Konwersja jednostki miary (proszę stworzyć szkielet funkcji, uwzględniający argumenty: informacja wejściowa, jednostka docelowa wg słownika jednostek np. 'szt'),
e) Zapis tablicy do ciągu znaków z elementami oddzielonymi przecinkami; Wejście: tablica, Wyjście: string: "element1, element2, element3".


5. Konwerter wykorzystując Słownik treści (3.a) mapuje zawartość Źródła na Szablon po nazwach kolumn, w trakcie mapowania wykonując operacje zapisane w Słowniku funkcji dla danej kolumny, wg podanej kolejności funkcji (możliwość sekwencyjnego przepuszczenia danej wejściowej przez kilka funkcji). W przypadku podania kilku Źródeł na wejściu Konwertera, są one otwierane i mapowane na 1 arkusz wynikowy po kolei (w przypadku gdyby drugie Źródło miało uzupełnione te same kolumny, co pierwsze źródło - zmapowane dane zostaną nadpisane, a w normalnej sytuacji gdy Źródła się uzupełniają w wyniku mapowania pliki zostaną automatycznie scalone na arkuszu docelowym np. pierwsze Źródło zawiera uzupełnioną kolumnę "Nazwa PL", drugie Źródło kolumnę "Nazwa DE" itp.).


6. Po przeprowadzeniu mapowania Źródeł na Szablon powstaje 1 plik wynikowy, który zapisywany jest w formacie xls i z nazwą: `<nazwa-szablonu>-<data-godzina>-converted`.


W takiej formie skrypt konwertujący będzie co prawda posiadać ograniczoną funkcjonalność, jednak będzie mieć potencjał rozwoju i wykorzystania przy scrapowaniu kolejnych stron www z wykorzystaniem webscraper.io. Dodatkowo dzięki możliwości wczytania kilku źródeł (np. z uzupełnionymi kolumnami dla różnych wersji językowych) będzie możliwe ich scalenie na jednym arkuszu wyjściowym. Odnośnie zakresu Funkcji na start - proponuję zacząć od najbardziej podstawowych czyli rozbicie tablicy JSON, walidacja string, number, url.