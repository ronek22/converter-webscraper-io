import json
from glob import glob
import argparse
import os
from typing import List

def open_and_get_arguments(file_path):
    with open(file_path, 'r') as f:
        data = json.load(f)
    
    data = data.get('configurations')[0].get('args')
    index = data.index('--match') + 1
    data[index] = f'"{data[index]}"'
    if data[1][0] == '[':
        data[1] = f'"{data[1]}"'
    return ' '.join(data)

def prepare_cmd(file_path):
    return f"python3 script.py {open_and_get_arguments(file_path)}"

def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument("--brand", type=str, required=True)
    args = parser.parse_args()

    return args.brand

if __name__ == "__main__":

    brand = parse()


    all_launch = [x for x in glob(f'sitemaps/{brand}/**/launch.json', recursive=True)]

    for launch in all_launch:
        cmd = prepare_cmd(launch) 
        os.system(cmd)
        print(f"DONE! {launch}")
