from glob import glob
import argparse
import pandas as pd
from pprint import pprint


class Row:
    def __init__(self, name, functions, values, comment):
        self.name = name
        self.functions = functions
        self.values = values
        self.comment = comment

    def __str__(self):
        return name

    

fix_column_name = lambda x: x.replace('*', '').strip()
clear_str = lambda x: x.replace('\n', ' ').strip() if not pd.isnull(x) else ''



def generate_report(scheme_file: str, skip_rows: int=3, drop_first_column: bool=True):
    for_url = pd.read_excel(scheme_file, index=False)
    url = for_url.columns[1]
    del for_url
    scheme = pd.read_excel(scheme_file, index=False,  skiprows=skip_rows)
    if drop_first_column:
        scheme.drop(scheme.columns[[0]], axis = 1, inplace = True) 

    columns = scheme.columns.values.tolist()
    functions = scheme.iloc[0].values.tolist()
    comments = scheme.iloc[1].values.tolist()
    to_fill = scheme.iloc[2].values.tolist()

    result = [Row(fix_column_name(name), clear_str(function), clear_str(fill), clear_str(comment))
    for name,function,fill,comment in zip(columns, functions, to_fill, comments) if (not pd.isnull(fill)) and (('Wartość stała' not in fill))]

    return result, url


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument("--brand", type=str, required=True)
    args = parser.parse_args()

    return args.brand

if __name__ == "__main__":

    brand = parse()


    all_launch = [x for x in glob(f'sitemaps/{brand}/**/szablon.xls', recursive=True)]
    all_launch.sort()
    pprint(all_launch)

    rep_file = open(f'reports/{brand}.md', 'w')


    for launch in all_launch:
        category = launch.split('/')[2].capitalize()
        report, url = generate_report(launch)
        rep_file.write(f"## {category}: {url}\n")
        rep_file.write("Column | Values | Comment | Functions\n--- | --- | --- | ---\n")
        for row in report:
            rep_file.write(f"{row.name} | {row.values} | {row.comment} | {row.functions}\n")
        rep_file.write("\n\n")

    rep_file.close()