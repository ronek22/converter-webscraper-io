from xlrd import open_workbook
from xlrd.sheet import Sheet
from xlutils.copy import copy
from glob import glob
from PyInquirer import style_from_dict, prompt

def get_sheet(path:str) -> Sheet:
    workbook = open_workbook(path, formatting_info=True)
    wb = copy(workbook)
    return workbook.sheet_by_index(0), wb

def get_index_of_value(sheet: Sheet, value:str) -> tuple:
    for row in range(sheet.nrows):
        for col in range(sheet.ncols):
            if sheet.cell_value(row, col) == value:
                return (row,col)
    return None

def find_all_brands():
    all_brands = [{'name':x.replace('sitemaps/', '')[:-1],'value':x.replace('sitemaps/', '')[:-1]} for x in glob('sitemaps/**/')]
    return all_brands

def find_all_schemes(brand:str):
    all_launch = [{'name': 'All schemes..', 'value': 'ALL'}]
    all_launch.extend([{'name':x,'value':x} for x in glob(f'sitemaps/{brand}/**/results/szablon.xls', recursive=True)])
    return all_launch

def main_menu():
    menu = {
        'type': 'list',
        'name': 'choice',
        'message': 'Wybierz producenta',
        'choices': find_all_brands()
    }

    answer = prompt(menu)
    return answer['choice']

def scheme_menu(brand:str):
    menu = {
        'type': 'list',
        'name': 'path',
        'message': 'Wybierz kategorie',
        'choices': find_all_schemes(brand)
    }

    answer = prompt(menu)
    return answer['path']

def input_column_name():
    menu = {
        'type': 'input',
        'name': 'column',
        'message': 'Podaj nazwe kolumny, ktora ma byc edytowana',
    }

    answer = prompt(menu)
    return answer['column']

def input_value():
    menu = {
        'type': 'input',
        'name': 'value',
        'message': 'Podaj edytowana tresc komorki',
    }

    answer = prompt(menu)
    return answer['value']

def edit_scheme(file, column, value):
    sheet, wb = get_sheet(file)
    index = get_index_of_value(sheet, column)
    cell = sheet.cell_value(index[0]+1, index[1])
    s = wb.get_sheet(0)
    s.write(index[0]+1, index[1], value)
    wb.save(file)


if __name__ == "__main__":

    choose = main_menu()
    file = scheme_menu(choose)
    column = input_column_name()
    value = input_value()

    if file == 'ALL':
        for scheme in [x['value'] for x in find_all_schemes(choose)][1:]:
            edit_scheme(scheme, column, value)
    else:
        edit_scheme(file, column, value)
