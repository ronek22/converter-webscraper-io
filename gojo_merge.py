import pandas as pd
from glob import glob

results = [pd.read_excel(x) for x in glob('./sitemaps/gojo/**/results.xlsx', recursive=True)]

final = pd.concat(results)
final = final.sort_values(by=['Numer artykułu'])

with pd.ExcelWriter('./sitemaps/gojo/result.xlsx') as writer:
        final.to_excel(writer, sheet_name="Merged", index=False, encoding='utf8', engine='xlsxwriter')