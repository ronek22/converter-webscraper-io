import argparse
import pandas as pd
import json
import pprint
from typing import List,Union
import re
import validators
from collections import OrderedDict
from json import JSONDecodeError

# RUN WITH: python3 script.py --source pl-pramol.csv en-pramol.csv de-pramol.csv --scheme scheme.xlsx --match "Nazwa produktu [PL]"

Number = Union[int, float]
ext = lambda p: p.split('.')[-1]
pp = pprint.PrettyPrinter(indent=4)
_print = lambda p: pp.pprint(p)


def rgx(check:str, value:str, all_occurencies:bool, group:int=0):
    """Return first occurences of matched regex"""
    
    if all_occurencies:
        r = re.findall(check, value)
        return ''.join(r)
    r = re.search(check, value)
    return r.group(group) if r else ""

def fix_column_name(value:str):
    value = value.replace('*', '')
    return value.strip()

def safe_split(text:str, sep):
    try:
        return text.split(sep)
    except AttributeError:
        return []

def read_scheme(scheme_file: str, skip_rows: int, drop_first_column: bool):
    scheme = pd.read_excel(scheme_file,skiprows=skip_rows)
    if drop_first_column:
        scheme.drop(scheme.columns[[0]], axis = 1, inplace = True) 
    columns = scheme.columns.values.tolist()
    functions = scheme.iloc[0].values.tolist()
    try:
        to_fill = scheme.iloc[2].values.tolist()
        to_fill = {fix_column_name(name):fill.replace('Wartość stała:','').strip()
        for name,fill in zip(columns, to_fill) if (not pd.isnull(fill)) and ('Wartość stała' in fill)}
    except IndexError:
        to_fill = []

    

    columns_struct = {fix_column_name(name):index for index,name in enumerate(columns)}
    columns_struct = OrderedDict(sorted(columns_struct.items(), key=lambda x: x[1]))
    functions_struct = {index:[x.strip() for x in safe_split(name,',')] for index,name in enumerate(functions)}
    units_struct = {}

    return columns_struct, functions_struct, units_struct, to_fill

def parsing():
    parser = argparse.ArgumentParser()
    parser.add_argument("--source", nargs="*", required=True) # MAIN DATA FILE
    parser.add_argument("--scheme", type=str, required=True) # SCHEME FILE
    parser.add_argument("--match", type=str, required=True)  # NAME OF COLUMN TO JOIN FOR OTHER TABLES
    parser.add_argument("--drop", type=bool, required=False, default=False) # AMOUNT OF COLUMNS TO DROP FROM SCHEME
    parser.add_argument("--skip_rows", type=int, required=False, default=0) # AMOUNT OF ROWS TO DROP FROM SCHEME
    parser.add_argument("--save_path", type=str, default="result.xlsx") 
    parser.add_argument("--match_length", type=int, default=None) # AMOUNT OF CHARS TO PASS TEST FOR JOINING BETWEEN TABLES
    parser.add_argument("--products", nargs="*", default=[]) # ROWS TO DROP FROM RESULTS TABLE
    parser.add_argument("--exclude", nargs="*", required=False, default=None)
    parser.add_argument("--require_columns", nargs="*", required=False, default=[])
    parser.add_argument("--reversed", type=bool, required=False, default=False)

    args = parser.parse_args()

    source_files = args.source  # by commas: args.source[0].split(',')
    source_files = [tuple(x.split(':')[:2]) for x in source_files]
    scheme_file = args.scheme
    drop = args.drop
    match = args.match
    skip_rows = args.skip_rows
    exclude = [tuple(x.split(":")) for x in args.exclude] if args.exclude else []

    # if any((ext(x) != 'csv' for x in [i[0] for i in source_files])):
    #     raise AttributeError("Converter can accept only .csv for source files")

    if ext(scheme_file) not in ['xls', 'xlsx']:
        raise AttributeError("Converter can accept only .xls or .xlsx file extenstion for scheme")


    return source_files, scheme_file, match, drop, skip_rows, args.save_path, args.match_length, args.products, args.require_columns, exclude, args.reversed

def find_corresponding_row(file, ind, match_length):
    if match_length:
        match_from_col_lookup = file[file.columns.values[ind-1]].apply(lambda x: x[:match_length])
        founded = file.loc[match_from_col_lookup==actual.get(match)[:match_length],  key]
    else:
        founded = file.loc[file[file.columns.values[ind-1]] == actual.get(match), key]

    return founded

def search_in_lookups(lookups, key, match_length):
    for csv, ind in lookups: # Check on other files 
        columns = list(map(lambda x: x.replace('-src', '') ,csv.columns.values.tolist()))
        if key in columns:
            founded = find_corresponding_row(csv, ind, match_length)
            if not founded.empty:
                actual[key] = next(filter(lambda x: not pd.isnull(x), founded.values.tolist()), None)
                value = actual[key]
                return value
    return None

def try_to_fill_column(row, key, columns):

    valid_key = toUnder(key)
    matched = [getattr(row, x, None) for x in columns if valid_key in x]
    result = next((x for x in matched if not pd.isnull(x)), None)

    if (not result) or (pd.isnull(result)):
        result = search_in_lookups(rest_csv, key, match_length)
    
    return result

def run_parametrized_function(func):
    start = func.index('(') + 1
    func = func[:start] + 'actual[key],' + func[start:]
    func = func.replace('“', '"').replace('”','"').replace(';', ',')
    result = eval(func)
    return result

def handle_source_files(source):
    source = source.replace('\\', '')
    if source[0] == '[' and source[-1] == ']':
        list_of_files = eval(source)
        return pd.concat((pd.read_csv(f, index_col=False, dtype=str) for f in list_of_files), ignore_index=True)
    return pd.read_csv(source, index_col=False, dtype=str)

#region Functions

def validateNumber(values:List[str], column:str) -> List[Number]:
    result = []
    return [rgx('\d+[.,]?\d*', x, False) for x in values]


def validateHTML(values, column):
    result = []
    for x in values:
        x = x.replace('</li>', ', -').replace('<li>','').replace('<br>', ', -')
        result.append(' '.join(x.split()))
    return result

def validateEAN(values, column):
    return values

def validateURL(values:List[str], column:str) -> List[str]:
    # not validate 
    return values
    
    result = []
    for x in values:
        if validators.url(x):
            result.append(x)

    return result

def convertWeightUnit(values, column):
    return values

def convertDimesionUnit(values, column):
    return values

def validateString(values:List[str], column:str, regex:str='.*', all_occurencies:bool=True) -> List[str]:
    """Validate list of strings using regex"""

    result = [rgx(regex, x, all_occurencies) for x in values]
    # remove not wanted chars
    not_wanted = ['➀', '➁', '➂']
    result = [word.translate({ord(c):'' for c in not_wanted}).strip() for word in result]
    return list(filter(None, result))

def json2Array(values:str, column:str, regex:str='') -> List[str]:
    try:
        data = json.loads(values)
    except TypeError:
        try:
            data= json.loads(*values)
        except JSONDecodeError:
            return values

    # get longer lenght key, because it's big chance that this key is attribute
    try:
        key = max(data[0].keys(), key=len)
    except IndexError:
        return ['']

    result = [x[key] for x in data]

    if regex:
        return validateString(result, regex)

    # remove duplicates
    result = list(set(result))

    return result

def array2String(values:List[str], column:str) -> str:
    if not values:
        return ''
    if type(values[0]) != str:
        return int(values[0]) 

    return ','.join(values)

def backToSpaces(value:str) -> str:
    return value.replace('_', ' ')

def toUnder(value:str) -> str:
    return value.replace(' ', '_').replace('[', '__').replace(']', '___').replace('-src', '')

def deleteWord(value:str, delete:str) -> str:
    return value.replace(delete, '')

def removeNull(value:str) -> str:
    return '' if value == '0' else value

def wordSplit(value:str, delimiter:str, index:int, endindex:int=0):
    if value == '"-"': return ''
    splitted = re.split('x|-|×|X', value)
    try:
        if endindex:
            try:
                res = f'{delimiter}'.join(splitted[index-1:endindex]).strip()
            except IndexError:
                res = splitted[index-1].strip()
        else:
            res = splitted[index-1].strip()
    except IndexError:
        if len(splitted) == 1 and index > 1:
            return splitted[0]
        return ''

    if index == 1 and len(splitted) == 1:
        return '1'

    return res


def prependList(values:List[str], add:str) -> List[str]:
    result = [f"{add}{value}" for value in values]
    return result

def cutString(value:str, begin:str='', end:str=''):
    lower = value.lower()
    try:
        begin = lower.index(begin) if begin else 0
    except ValueError:
        return ''
    try:
        end = lower.index(end) if end else len(lower)
    except ValueError:
        end = len(lower)
    return value[begin:end]

def make_options(value:str, delimiter:int):
    return [x.strip() for x in value.split(chr(delimiter))]

# def make_variants(value:str):
#     return list(filter(None, value.split(' ')))




call_dict = {
    'validateNumber': validateNumber,
    'validateString': validateString,
    'validateHTML': validateHTML,
    'validateEAN': validateEAN,
    'validateURL': validateURL,
    'convertWeightUnit': convertWeightUnit,
    'convertDimensionUnit': convertDimesionUnit,
    'json2Array': json2Array,
    'array2String': array2String,
}

#endregion

    
if __name__ == "__main__":

    #region CONFIGURATION
    # PARSING
    source_files, scheme_file, match, drop, skip_rows, save_path, match_length, products, require_columns, exclude, revers = parsing()

    # SCHEME DATA
    columns, functions, units, to_fill = read_scheme(scheme_file, skip_rows, drop)

    # MAIN DATA FILE, THAT WILL BE LOOP THROUGH
    csv = handle_source_files(source_files.pop(0)[0])
    if products:
        csv = csv[csv['Numer artykułu'].isin(products)]

    if exclude:
        for column, value in exclude:
            csv = csv[csv[column] != value]
        
    csv.rename(columns=lambda x: toUnder(x), inplace=True)
    rest_csv = [(handle_source_files(x),int(ind)) for x, ind in source_files]
    
    # RESULT TABLE
    result = pd.DataFrame(columns=columns)

    #endregion

    # LOOP THROUGH MAIN DATA FILE
    for row in csv.itertuples():
        options = None
        options_key = None
        actual = {} # store info about actual processed row
        column_keys = reversed(columns.keys()) if revers else columns.keys()
        for key in column_keys:

            if key in to_fill:
                actual[key] = to_fill.get(key)
                continue

            actual[key] = getattr(row, toUnder(key), None)
            if pd.isnull(actual[key]):
                actual[key] = try_to_fill_column(row, key, csv.columns.tolist())

            if (key in require_columns) and (pd.isnull(actual[key])):
                actual = {}
                break
            
            # TODO: REFACTORING
            if not pd.isnull(actual[key]):
                column_id = columns[key]
                functions_to_apply = functions[column_id]
                if type(actual[key]) != list:
                    actual[key] = [actual[key]]

                for func in functions_to_apply:
                    if '(' in func:
                        if 'options' in func:
                            if actual[key]:
                                options = run_parametrized_function(func)
                                options_key = key
                        else:
                            actual[key] = run_parametrized_function(func)
                    else:
                        actual[key] = call_dict[func](values=actual[key], column=key)
        
        if options:
            while options:
                actual[options_key] = options.pop()
                result = result.append(actual, ignore_index=True) if actual else result
        else:
            result = result.append(actual, ignore_index=True) if actual else result

    result = result.sort_values(by=['Numer artykułu'])
                

    with pd.ExcelWriter(save_path) as writer:
        result.to_excel(writer, sheet_name="Merged", index=False, encoding='utf8', engine='xlsxwriter')


