import pandas as pd
from glob import glob

name = lambda x: x.split('/')[-1][7:-4].replace('-', ' ').capitalize()
def clear_number(x:str):
    try:
        if not pd.isna(x):
            if x.startswith('Numer produktu 3M'):
                return x.replace('Numer produktu 3M', '').strip()
            return str(x).split()[2]
    except IndexError:
        print(f"Index Error: {x.split()}")

paths = glob('./from_site/*.csv')
data = [(pd.read_csv(x),name(x)) for x in paths]

# setting Kategoria produktu
for csv, category in data:
    csv['Kategoria produktu'] = category

merge = pd.concat([x[0] for x in data])
# get rid of 3M ID or Numer produktu
merge['Numer artykułu'] = merge['Numer artykułu'].fillna(merge['id_3m'])
merge['Numer artykułu'] = merge['Numer artykułu'].map(clear_number)
merge['Dostępne warianty'] = merge['Dostępne warianty'].map(lambda x: [v['Dostępne warianty'] for v in eval(x)] if x else []) 
merge['Numer artykułu'] = merge['Numer artykułu'].fillna(merge['Dostępne warianty'])
merge['Numer artykułu'] = merge['Numer artykułu'].map(lambda x: x[0].split()[2] if (type(x)==list and x) else x)
merge['Dostępne warianty'] = merge['Dostępne warianty'].map(lambda x: ','.join([v.split()[2] for v in x]))

merge.drop(merge.iloc[:, 0:4], inplace=True, axis=1)
merge = merge.replace('\n',' ', regex=True)
merge.to_csv('merge-3m-final.csv', index=False)

