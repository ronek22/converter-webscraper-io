import pandas as pd
# import requests
from requests_html import HTMLSession
from bs4 import BeautifulSoup as bs
from urllib.parse import quote
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

session = HTMLSession()
# session.headers.update({'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36'})
SEARCH = 'https://www.3mpolska.pl/3M/pl_PL/firma-pl/wyszukaj/'
link = lambda x: f"{SEARCH}?Ntt={quote(x)}"
driver = webdriver.Chrome(ChromeDriverManager().install())



def get_from_www(query):
    # payload = {'Ntt': query}
    # search_site = session.get(SEARCH, params=payload, timeout=10)
    driver.get(link(query))

base = pd.read_excel('sitemaps/3M/result.xlsx')


pairs  = ((number,name) for number, name in zip(base['Numer artykułu'], base['Nazwa produktu [PL]']))

for index, value in enumerate(pairs):
    number, name = value
    get_from_www(number)
    print(f"{index}: {value} Done")
    

