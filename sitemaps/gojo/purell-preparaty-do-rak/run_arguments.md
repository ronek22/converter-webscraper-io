# Instructions

**Run from top-level directory, where script.py exist**

```
python3 script.py --source sitemaps/gojo/purell-preparaty-do-rak/results/purell-preparaty-do-rak-pl.csv sitemaps/gojo/purell-preparaty-do-rak/results/purell-preparaty-do-rak-en.csv:5 sitemaps/gojo/purell-preparaty-do-rak/results/purell-preparaty-do-rak-de.csv:5 --scheme sitemaps/gojo/purell-preparaty-do-rak/results/szablon_formularz_produkty.xls --match "Numer artykułu" --drop True --skip_rows 3 --save_path sitemaps/gojo/purell-preparaty-do-rak/results/results_new.xlsx --match_length 7
```